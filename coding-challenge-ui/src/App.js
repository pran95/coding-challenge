import React, { useState } from 'react';
import styled from 'styled-components';
import Table from "./table";

const AppWrapper = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: #cccccc;
`
const TableWrapper = styled.div`
  padding: 10vh;
  background-color: #cccccc;
`

const AppHeader = styled.header`
  background-color: white;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0rem 2rem;
`

const HeaderText = styled.h1`
  font-family: 'Roboto', sans-serif;
`

const Username = styled.span`
  font-family: 'Roboto', sans-serif;
`

const App = () => {
    const [user, setUser] = useState(null)
    const [orders, setOrders] = useState([])
    const [stores, setStores] = useState([])
    React.useEffect(() => {
        fetch('http://localhost:8080/user')
            .then(results => results.json())
            .then(data => {
                setUser(data)
            });
        fetch('http://localhost:8080/sales')
            .then(results => results.json())
            .then(data => {
                setOrders(data)
            });
        fetch('http://localhost:8080/stores')
            .then(results => results.json())
            .then(data => {
                setStores(data)
            });
    }, []);
    // let filter = orders.filter(d => d.orderId === "OROL1GX1Z7")
    console.log(orders)
    console.log(stores)
    return (
        <>
            <AppWrapper>
                <AppHeader>
                    <HeaderText>Analytics Dashboard</HeaderText>
                    <Username>Welcome, {user ? user.firstName : 'Guest'}!</Username>
                </AppHeader>

                <TableWrapper> <Table orders={orders} stores={stores}/> </TableWrapper>
            </AppWrapper>

        </>
    );
}

export default App;
