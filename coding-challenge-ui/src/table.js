import React from "react";
import DataTable from "react-data-table-component";
import Card from "@material-ui/core/Card";
import SortIcon from "@material-ui/icons/ArrowDownward";


const customStyles = {
  headCells: {
    style: {
      backgroundColor:'#F0F8FF',
      fontWeight:"bold",
      color:"grey",
      fontSize:"0.9em"
    },
  },

};
const columns = [
  {
    name: "MARKETPLACE",
    selector: "marketplace",
    sortable: false
  },
  {
    name: "STORE",
    selector: "shopName",
    sortable: false
  },
  {
    name: "ORDER ID",
    selector: "orderId",
    sortable: false
  },
  {
    name: "ORDER VALUE",
    selector: "orderValue",
    sortable: false,
    center: true
  },
  {
    name: "ITEMS",
    selector: "items",
    sortable: true,
    center: true

  },
  {
    name: "DESTINATION",
    selector: "destination",
    sortable: false
  },
  {
    name: "DAYS OVERDUE",
    selector: "days",
    sortable: true,
    center: true,
    cell: row => <div data-tag="allowRowEvents">
                     <div style={{color: "red" }}>{row.days}</div>
                </div>
  }
];
const tableData = [
  {
    marketplace: "🇬🇧 Ebay",
    shopName: "Snack Co.",
    orderId: "ORGG7YHW5F",
    orderValue: "$49.99",
    items: 8,
    destination: "LON UK, E1 6AN",
    days: 44,
  },
  {
    marketplace: "🇦🇺 Amazon",
    shopName: "Shoe Plus",
    orderId: "OR2E47PHFB",
    orderValue: "$120",
    items: 1,
    destination: "VIC AU, 3140",
    days: 23,
  },
  {
    marketplace: "🇺🇸 Amazon",
    shopName: "Great Jeans",
    orderId: "OROL1GX1Z7",
    orderValue: "$75.50",
    items: 1,
    destination: "LA US, 90001",
    days: 15,
  },
  {
    marketplace: "🇦🇺 Catch",
    shopName: "Sheetly",
    orderId: "OR9JYZ6D9T",
    orderValue: "$30.00",
    items: 2,
    destination: "NSW AU, 2072",
    days: 11,
  },
  {
    marketplace: "🇦🇺 Amazon",
    shopName: "Shoe Plus",
    orderId: "ORDPGJ0JQM",
    orderValue: "$69.99",
    items: 1,
    destination: "QLD AU, 4505",
    days: 8,
  },
  ];

const Table = (orders, stores)=> {
  return (
    <div style={{fontFamily: "sans-serif"}}>
      <Card>
        <DataTable
          title="Overdue Orders"
          columns={columns}
          data={tableData}
          defaultSortFieldId={1}
          sortIcon={<SortIcon />}
          pagination
          customStyles={customStyles}
        />
      </Card>
    </div>
  );
}
export default Table
