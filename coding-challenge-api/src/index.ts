import express, { Request, Response } from "express";
import cors from "cors";
const app = express();
const port = 8080;
const ordersFilePath ='./data/orders.csv'
const storesFilePath = './data/stores.csv'
const csv=require('csvtojson')

app.use(cors());
app.get("/user", getUser);

app.get("/sales",(req: any, res: any) => {

  try{

    csv()
        .fromFile(ordersFilePath)
        .then((jsonObj: any)=>{
          res.json(jsonObj);
        })
  }
  catch(error){
    console.trace(error);
    res.sendStatus(500)
  }
});
app.get("/stores", (req: any, res: any) => {
  try{
    csv()
        .fromFile(storesFilePath)
        .then((jsonObj: any)=>{
          res.json(jsonObj);
        })
  }
  catch(error){
    console.trace(error);
    res.sendStatus(500)
  }
});

app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});

export function getUser(req: any, res: any) {
  res.json({
    firstName: "Jane",
    lastName: "Doe",
    email: "janedoe@email.com",
    id: 1,
  });
}
